use crate::{Score, Sizes};

#[derive(Component)]
pub struct ScoreText;

use bevy::prelude::*;

pub fn display_score(mut commands: Commands, asset_server: Res<AssetServer>, sizes: Res<Sizes>) {
    let font_size = sizes.cell * 2.;
    commands.spawn((
        TextBundle::from_sections([
            TextSection::new(
                "Points: ",
                TextStyle {
                    font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                    font_size,
                    color: Color::DARK_GRAY,
                },
            ),
            TextSection::from_style(TextStyle {
                font: asset_server.load("fonts/FiraMono-Bold.ttf"),
                font_size,
                color: Color::RED,
            }),
            TextSection::new(
                "\nLines: ",
                TextStyle {
                    font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                    font_size,
                    color: Color::DARK_GRAY,
                },
            ),
            TextSection::from_style(TextStyle {
                font: asset_server.load("fonts/FiraMono-Bold.ttf"),
                font_size,
                color: Color::RED,
            }),
            TextSection::new(
                "\nLevel: ",
                TextStyle {
                    font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                    font_size,
                    color: Color::DARK_GRAY,
                },
            ),
            TextSection::from_style(TextStyle {
                font: asset_server.load("fonts/FiraMono-Bold.ttf"),
                font_size,
                color: Color::RED,
            }),
        ]),
        ScoreText,
    ));
}
pub fn display_game_over_score(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    scoreboard: Res<Score>,
    sizes: Res<Sizes>,
) {
    let font_size = sizes.cell * 2.;
    let text_color = Color::DARK_GRAY;
    let score_color = Color::RED;
    commands
        .spawn(NodeBundle {
            style: Style {
                width: Val::Percent(100.),
                height: Val::Percent(100.),
                align_items: AlignItems::Center,
                justify_content: JustifyContent::Center,
                ..default()
            },
            ..default()
        })
        .with_children(|parent| {
            parent.spawn(TextBundle::from_sections([
                TextSection::new(
                    "Lines: ",
                    TextStyle {
                        font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                        font_size,
                        color: text_color,
                    },
                ),
                TextSection::new(
                    scoreboard.lines.to_string(),
                    TextStyle {
                        font: asset_server.load("fonts/FiraMono-Medium.ttf"),
                        font_size,
                        color: score_color,
                    },
                ),
                TextSection::new(
                    "\nScore: ",
                    TextStyle {
                        font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                        font_size,
                        color: text_color,
                    },
                ),
                TextSection::new(
                    scoreboard.points.to_string(),
                    TextStyle {
                        font: asset_server.load("fonts/FiraMono-Medium.ttf"),
                        font_size,
                        color: score_color,
                    },
                ),
                TextSection::new(
                    "\nPress R to retry\nPress Q to quit\nPress O for options",
                    TextStyle {
                        font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                        font_size,
                        color: text_color,
                    },
                ),
            ]));
        });
}

pub fn display_menu(mut commands: Commands, asset_server: Res<AssetServer>, sizes: Res<Sizes>) {
    let font_size = sizes.cell * 2.;
    let text_color = Color::DARK_GRAY;
    commands
        .spawn(NodeBundle {
            style: Style {
                width: Val::Percent(100.),
                height: Val::Percent(100.),
                align_items: AlignItems::Center,
                justify_content: JustifyContent::Center,
                ..default()
            },
            ..default()
        })
        .with_children(|parent| {
            parent.spawn(TextBundle::from_sections([TextSection::new(
                "\nPress Space to play\nPress Q to quit\nPress O for options",
                TextStyle {
                    font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                    font_size,
                    color: text_color,
                },
            )]));
        });
}

pub fn display_options(mut commands: Commands, asset_server: Res<AssetServer>, sizes: Res<Sizes>) {
    let font = asset_server.load("fonts/FiraSans-Bold.ttf");
    let font_size = sizes.cell;
    let text_color = Color::DARK_GRAY;
    commands
        .spawn(NodeBundle {
            style: Style {
                width: Val::Percent(100.),
                height: Val::Percent(100.),
                top: Val::Px(100.),
                align_items: AlignItems::Start,
                justify_content: JustifyContent::Center,
                ..default()
            },
            ..default()
        })
        .with_children(|parent| {
            parent.spawn(TextBundle::from_sections([TextSection::new(
                "W - Sonic drop\nSpace - Hard drop\nS - Soft drop\nA - Move tetromino left\nD - Move tetromino right\nJ - Rotate π/2 / 90° left\nK - Rotate π / 180°\nL - Roate π/2 / 90° right",
                TextStyle {
                    font,
                    font_size,
                    color: text_color,
                },
            )]));
        });
}
