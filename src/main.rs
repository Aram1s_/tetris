use bevy::{
    asset::AssetMetaCheck,
    diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin},
    prelude::*,
    winit::{UpdateMode, WinitSettings},
};

mod block;
mod preview;
mod resize;
mod tetromino;
mod ui;
mod util;
mod walls;

use block::Block;
use preview::{Preview, PreviewBlock};
use resize::ResizePlugin;
use tetromino::{
    calculate_hard_drop, can_move, can_rotate, despawn_tetromino, move_tetromino, rotate_cell,
    rotate_tetromino, Cell, PlaceDelay, Tetromino, TetrominoDespawned, TetrominoKind, PLACE_DELAY,
};
use ui::{display_game_over_score, display_menu, display_options, display_score, ScoreText};
use util::{debug, exit, grid_to_world, log_resolution, teardown, world_to_grid};
use walls::{Collider, WallBundle, WallLocation, Walls};

const WIDTH: usize = 10;
const HEIGHT: usize = 20;

fn main() {
    App::new()
        // Temporary fixes
        // Removes requests for .meta files for web
        .insert_resource(AssetMetaCheck::Never)
        // Fix for errors and performance issues on Firefox
        .insert_resource(WinitSettings {
            focused_mode: UpdateMode::Continuous,
            unfocused_mode: UpdateMode::Continuous,
        })
        //
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                title: "Tetris".into(),
                canvas: Some("#game_canvas".into()),
                ..default()
            }),
            ..default()
        }))
        .add_plugins((
            #[cfg(debug_assertions)]
            FrameTimeDiagnosticsPlugin,
            #[cfg(debug_assertions)]
            LogDiagnosticsPlugin::default(),
            ResizePlugin,
        ))
        .insert_resource(ClearColor(Color::WHITE))
        .add_event::<TetrominoDespawned>()
        .insert_resource(NextTetromino(TetrominoKind::new()))
        .insert_resource(Sizes {
            cell: 40.,
            wall: 40. / 5.,
        })
        .insert_resource(PlaceDelay {
            timer: Timer::new(
                std::time::Duration::from_millis(PLACE_DELAY),
                TimerMode::Once,
            ),
        })
        .init_state::<GameState>()
        .add_systems(Startup, camera_setup)
        .add_systems(OnEnter(GameState::Menu), display_menu)
        .add_systems(Update, menu_keyboard.run_if(in_state(GameState::Menu)))
        .add_systems(
            OnEnter(GameState::Playing),
            (timer_setup, setup, display_score),
        )
        .add_systems(
            Update,
            (
                (
                    can_move,
                    move_tetromino,
                    can_rotate,
                    rotate_tetromino,
                    despawn_tetromino,
                    clear_lines,
                    update_score,
                    calculate_hard_drop,
                )
                    .chain(),
                debug,
            )
                .run_if(in_state(GameState::Playing)),
        )
        .add_systems(OnExit(GameState::Playing), teardown)
        .add_systems(OnEnter(GameState::Options), display_options)
        .add_systems(
            Update,
            options_keyboard.run_if(in_state(GameState::Options)),
        )
        .add_systems(OnExit(GameState::Options), teardown)
        .add_systems(
            Update,
            gameover_keyboard.run_if(in_state(GameState::GameOver)),
        )
        .add_systems(OnExit(GameState::GameOver), teardown)
        .add_systems(OnExit(GameState::Menu), (teardown, log_resolution))
        .add_systems(OnEnter(GameState::GameOver), display_game_over_score)
        .add_systems(OnEnter(GameState::Quit), exit)
        .add_systems(Update, bevy::window::close_on_esc)
        .run()
}

#[derive(Clone, Copy, Default, Eq, PartialEq, Debug, Hash, States)]
enum GameState {
    #[default]
    Menu,
    Options,
    Playing,
    GameOver,
    Quit,
}

#[derive(Component)]
struct Wall;

#[derive(Resource)]
pub enum TetrominoRotation {
    Neutral,
    Left,
    Right,
    Flip,
}

#[derive(Resource)]
pub struct Sizes {
    cell: f32,
    wall: f32,
}

#[derive(Resource)]
pub struct BlockGrid([[Option<Entity>; WIDTH]; HEIGHT]);

#[derive(Resource)]
pub struct NextTetromino(TetrominoKind);

#[derive(Resource, Clone)]
pub struct Score {
    points: u32,
    lines: u8,
}

fn timer_setup(mut timer: ResMut<PlaceDelay>) {
    timer.timer.pause();
    timer.timer.reset();
}

fn camera_setup(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}

fn setup(mut commands: Commands, sizes: Res<Sizes>) {
    commands.spawn((WallBundle::new(WallLocation::Left, &sizes), Wall));
    commands.spawn((WallBundle::new(WallLocation::Right, &sizes), Wall));
    commands.spawn((WallBundle::new(WallLocation::Bottom, &sizes), Wall));

    Tetromino::spawn(&mut commands, TetrominoKind::new(), sizes.cell);
    let t = TetrominoKind::new();
    Preview::spawn_preview(&mut commands, &t, sizes.cell);
    commands.insert_resource(NextTetromino(t));

    commands.insert_resource(BlockGrid([[None; WIDTH]; HEIGHT]));
    commands.insert_resource(Score {
        points: 0,
        lines: 0,
    });
}

fn clear_lines(
    mut commands: Commands,
    mut blocks: Query<(&mut Transform, &mut Block)>,
    mut score: ResMut<Score>,
    mut block_grid: ResMut<BlockGrid>,
    sizes: Res<Sizes>,
    mut ev_despawned: EventReader<TetrominoDespawned>,
) {
    if ev_despawned.is_empty() {
        return;
    }
    ev_despawned.clear();

    let mut count = 0;
    let mut to_move = vec![];

    for (i, row) in block_grid.0.iter_mut().enumerate().rev() {
        if row.iter().flatten().count() == WIDTH {
            for x in row {
                commands.entity(x.unwrap()).despawn();
                *x = None;
            }
            count += 1;
            to_move.push(i);
        }
    }

    for m in to_move {
        for i in m..HEIGHT - 1 {
            block_grid.0[i] = block_grid.0[i + 1];
            for (mut t, mut b) in &mut blocks {
                if b.y as usize == i + 1 {
                    b.y -= 1;
                    t.translation.y -= sizes.cell;
                }
            }
        }
    }

    match count {
        1 => score.points += 100,
        2 => score.points += 300,
        3 => score.points += 500,
        4 => score.points += 800,
        _ => (),
    }
    score.lines += count;
}

fn update_score(mut query: Query<&mut Text, With<ScoreText>>, score: Res<Score>) {
    query.single_mut().sections[1].value = score.points.to_string();
    query.single_mut().sections[3].value = score.lines.to_string();
    query.single_mut().sections[5].value = (score.lines / 10 + 1).to_string();
}

fn gameover_keyboard(
    mut next_state: ResMut<NextState<GameState>>,
    keyboard_input: Res<ButtonInput<KeyCode>>,
) {
    if keyboard_input.just_pressed(KeyCode::KeyR) {
        next_state.set(GameState::Playing);
    } else if keyboard_input.just_pressed(KeyCode::KeyQ) {
        next_state.set(GameState::Quit);
    }
}

fn menu_keyboard(
    mut next_state: ResMut<NextState<GameState>>,
    keyboard_input: Res<ButtonInput<KeyCode>>,
) {
    if keyboard_input.just_pressed(KeyCode::Space) {
        next_state.set(GameState::Playing);
    } else if keyboard_input.just_pressed(KeyCode::KeyQ) {
        next_state.set(GameState::Quit);
    } else if keyboard_input.just_pressed(KeyCode::KeyO) {
        next_state.set(GameState::Options);
    }
}

fn options_keyboard(
    mut next_state: ResMut<NextState<GameState>>,
    keyboard_input: Res<ButtonInput<KeyCode>>,
) {
    if keyboard_input.just_pressed(KeyCode::Space) {
        next_state.set(GameState::Playing);
    }
}
