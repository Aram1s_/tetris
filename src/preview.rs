use crate::tetromino::{Tetromino, TetrominoKind};
use bevy::prelude::*;

#[derive(Component)]
pub struct Preview {
    pub kind: TetrominoKind,
    pub x: f32,
    pub y: f32,
}

#[derive(Component)]
pub struct PreviewBlock {
    pub x: f32,
    pub y: f32,
}

impl Preview {
    pub fn spawn_preview(commands: &mut Commands, kind: &TetrominoKind, cell_size: f32) {
        let (sprites, _) = Tetromino::new_sprites(kind, cell_size);
        commands
            .spawn((
                Self::new_spatial_bundle(kind, cell_size),
                Preview {
                    kind: kind.clone(),
                    x: Self::offset(kind).x,
                    y: 0.,
                },
            ))
            .with_children(|p| {
                for sprite in sprites {
                    p.spawn((
                        sprite.to_owned(),
                        PreviewBlock {
                            x: sprite.transform.translation.x / cell_size,
                            y: sprite.transform.translation.y / cell_size,
                        },
                    ));
                }
            });
    }
    fn new_spatial_bundle(kind: &TetrominoKind, cell_size: f32) -> SpatialBundle {
        let offset = Self::offset(kind) * cell_size;
        SpatialBundle::from_transform(Transform::from_xyz(offset.x, offset.y, offset.z))
    }
    fn offset(kind: &TetrominoKind) -> Vec3 {
        let mut offset = Vec3::new(10., 0., 0.);
        match kind {
            TetrominoKind::L | TetrominoKind::J => offset.y -= 1.,
            _ => (),
        }
        offset
    }
}
