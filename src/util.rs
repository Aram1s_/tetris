use bevy::prelude::*;
use crate::BlockGrid;

pub fn grid_to_world((x, y): (u8, u8), cell_size: f32) -> (f32, f32) {
    ((x as f32 - 4.) * cell_size, (y as f32 - 9.5) * cell_size)
}

pub fn world_to_grid((x, y): (f32, f32), cell_size: f32) -> (u8, u8) {
    ((x / cell_size + 4.) as u8, (y / cell_size + 9.5) as u8)
}

pub fn debug(block_grid: Res<BlockGrid>, keyboard_input: Res<ButtonInput<KeyCode>>) {
    if !keyboard_input.just_pressed(KeyCode::KeyP) {
        return;
    }
    println!("the block grid:");
    for line in block_grid.0.iter() {
        println!(
            "{}",
            line.iter()
                .map(|m| if m.is_some() { "#" } else { " " })
                .collect::<String>()
        );
    }
}

pub fn log_resolution(resolution: Query<&Window>) {
    let height = &resolution.single().resolution.height();
    let width = &resolution.single().resolution.width();
    info!("width: {}, height: {}", width, height);
    info!(
        "cell size: {}",
        ((height / 24.).floor()).min((width / 22.).floor())
    );
}

pub fn teardown(mut commands: Commands, entities: Query<Entity, (Without<Window>, Without<Camera>)>) {
    for entity in &entities {
        commands.entity(entity).despawn();
    }
}

pub fn exit(mut commands: Commands, entities: Query<Entity>) {
    for entity in &entities {
        commands.entity(entity).despawn();
    }
}
