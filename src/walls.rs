use crate::Sizes;
use bevy::prelude::*;

const WALL_COLOR: Color = Color::BLACK;
const HEIGHT: f32 = 20.;
const WIDTH: f32 = 10.;

#[derive(Component)]
pub struct Collider;

#[derive(Bundle)]
pub struct WallBundle {
    sprite_bundle: SpriteBundle,
    collider: Collider,
}

pub enum WallLocation {
    Left,
    Right,
    Bottom,
}

impl WallLocation {
    pub fn position(&self, walls: &Walls) -> Vec2 {
        let pos = match self {
            WallLocation::Left => Vec2::new(walls.left, 0.),
            WallLocation::Right => Vec2::new(walls.right, 0.),
            WallLocation::Bottom => Vec2::new(0., walls.bottom),
        };
        pos + walls.offset
    }

    pub fn size(&self, walls: &Walls, sizes: &Res<Sizes>) -> Vec2 {
        let arena_height = walls.top - walls.bottom;
        let arena_width = walls.right - walls.left;
        assert!(arena_height > 0.0);
        assert!(arena_width > 0.0);

        match self {
            WallLocation::Left | WallLocation::Right => {
                Vec2::new(sizes.wall, arena_height + sizes.wall)
            }
            WallLocation::Bottom => {
                Vec2::new(arena_width + sizes.wall, sizes.wall)
            }
        }
    }
}

pub struct Walls {
    right: f32,
    left: f32,
    top: f32,
    bottom: f32,
    offset: Vec2,
}

impl Walls {
    pub fn new(sizes: &Res<Sizes>) -> Walls {
        Walls {
            right: sizes.cell * WIDTH / 2. + sizes.wall / 2.,
            left: -sizes.cell * WIDTH / 2. - sizes.wall / 2.,
            top: sizes.cell * HEIGHT / 2. + sizes.wall / 2.,
            bottom: -sizes.cell * HEIGHT / 2. - sizes.wall / 2.,
            offset: Vec2::new(sizes.cell / 2., 0.),
        }
    }
}

impl WallBundle {
    pub fn new(location: WallLocation, sizes: &Res<Sizes>) -> WallBundle {
        let walls = Walls::new(sizes);
        WallBundle {
            sprite_bundle: SpriteBundle {
                transform: Transform {
                    translation: location.position(&walls).extend(0.0),
                    scale: location.size(&walls, sizes).extend(1.0),
                    ..default()
                },
                sprite: Sprite {
                    color: WALL_COLOR,
                    ..default()
                },
                ..default()
            },
            collider: Collider,
        }
    }
}
