use crate::{
    world_to_grid, Block, BlockGrid, Collider, GameState, NextTetromino, Preview, Score,
    Sizes,
};
use bevy::{
    math::bounding::{Aabb2d, IntersectsVolume},
    prelude::*,
};

pub const PLACE_DELAY: u64 = 500;

#[derive(Event, Default)]
pub struct TetrominoDespawned;

#[derive(Clone)]
pub enum TetrominoKind {
    I,
    O,
    T,
    J,
    L,
    S,
    Z,
}

#[derive(Debug, PartialEq)]
pub enum Rotation {
    Neutral,
    Left,
    Right,
    Flip,
}

pub struct Directions {
    pub left: bool,
    pub right: bool,
    pub down: bool,
}

#[derive(Component)]
pub struct Tetromino {
    pub kind: TetrominoKind,
    pub speed: f32,
    pub moveable: Directions,
    pub can_rotate: Directions,
    pub drop_y_pos: f32,
    pub pos: Vec2,
    pub rotation: Rotation,
}

#[derive(Component)]
pub struct Cell {
    pub x: i8,
    pub y: i8,
}

#[derive(Resource)]
pub struct PlaceDelay {
    pub timer: Timer,
}

impl Rotation {
    fn rotate(&mut self, rotation: Rotation) {
        match self {
            Rotation::Neutral => match rotation {
                Rotation::Neutral => (),
                Rotation::Left => *self = Rotation::Left,
                Rotation::Right => *self = Rotation::Right,
                Rotation::Flip => *self = Rotation::Flip,
            },

            Rotation::Left => match rotation {
                Rotation::Neutral => *self = Rotation::Left,
                Rotation::Left => *self = Rotation::Flip,
                Rotation::Right => *self = Rotation::Neutral,
                Rotation::Flip => *self = Rotation::Right,
            },

            Rotation::Right => match rotation {
                Rotation::Neutral => *self = Rotation::Right,
                Rotation::Left => *self = Rotation::Neutral,
                Rotation::Right => *self = Rotation::Flip,
                Rotation::Flip => *self = Rotation::Left,
            },

            Rotation::Flip => match rotation {
                Rotation::Neutral => *self = Rotation::Flip,
                Rotation::Left => *self = Rotation::Right,
                Rotation::Right => *self = Rotation::Left,
                Rotation::Flip => *self = Rotation::Neutral,
            },
        }
    }
}

impl TetrominoKind {
    fn color(&self) -> Color {
        match self {
            TetrominoKind::I => Color::CYAN,
            TetrominoKind::O => Color::YELLOW,
            TetrominoKind::T => Color::PURPLE,
            TetrominoKind::J => Color::BLUE,
            TetrominoKind::L => Color::ORANGE,
            TetrominoKind::S => Color::GREEN,
            TetrominoKind::Z => Color::RED,
        }
    }

    pub fn new() -> TetrominoKind {
        use rand::Rng;
        match rand::thread_rng().gen_range(0..7) {
            0 => TetrominoKind::I,
            1 => TetrominoKind::O,
            2 => TetrominoKind::T,
            3 => TetrominoKind::J,
            4 => TetrominoKind::L,
            5 => TetrominoKind::S,
            6 => TetrominoKind::Z,
            _ => panic!("Invalid kind of tetromino generated."),
        }
    }
    pub fn offset(&self) -> f32 {
        match self {
            TetrominoKind::L | TetrominoKind::J => 8.5,
            _ => 9.5,
        }
    }
}

impl Tetromino {
    pub fn spawn(commands: &mut Commands, kind: TetrominoKind, sizes: f32) {
        let (sprites, cell_offset) = Self::new_sprites(&kind, sizes);
        let tetromino_offset = kind.offset();

        let mut ids = Vec::new();
        commands
            .spawn((
                Self::new_spatialbundle(&kind, sizes),
                Tetromino {
                    kind,
                    speed: 10.,
                    moveable: Directions {
                        right: true,
                        left: true,
                        down: true,
                    },
                    can_rotate: Directions {
                        right: true,
                        left: true,
                        down: true,
                    },
                    drop_y_pos: 0.,
                    pos: Vec2::new(0., tetromino_offset),
                    rotation: Rotation::Neutral,
                },
            ))
            .with_children(|p| {
                for (sprite, cell_offset) in sprites.iter().zip(cell_offset) {
                    ids.push(
                        p.spawn((
                            sprite.to_owned(),
                            Cell {
                                x: cell_offset.x as i8,
                                y: cell_offset.y as i8,
                            },
                        ))
                        .id(),
                    );
                }
            });
    }

    fn new_spatialbundle(kind: &TetrominoKind, sizes: f32) -> SpatialBundle {
        SpatialBundle::from_transform(Transform::from_xyz(0., kind.offset() * sizes, 0.))
    }

    pub fn new_sprites(kind: &TetrominoKind, sizes: f32) -> ([SpriteBundle; 4], [Vec2; 4]) {
        let rf = SpriteBundle {
            transform: Transform::from_scale(Vec3::new(sizes, sizes, 1.)),
            sprite: Sprite {
                color: kind.color(),
                ..default()
            },
            ..default()
        };

        let mut bundle = [rf.to_owned(), rf.clone(), rf.clone(), rf.clone()];

        let offset = match kind {
            TetrominoKind::I => [
                Vec2 { x: 1., y: 0. },
                Vec2 { x: 0., y: 0. },
                Vec2 { x: -1., y: 0. },
                Vec2 { x: -2., y: 0. },
            ],
            TetrominoKind::O => [
                Vec2 { x: 0., y: 0. },
                Vec2 { x: 0., y: -1. },
                Vec2 { x: -1., y: 0. },
                Vec2 { x: -1., y: -1. },
            ],
            TetrominoKind::T => [
                Vec2 { x: 0., y: 0. },
                Vec2 { x: 0., y: -1. },
                Vec2 { x: -1., y: 0. },
                Vec2 { x: 1., y: 0. },
            ],
            TetrominoKind::J => [
                Vec2 { x: 0., y: 1. },
                Vec2 { x: 0., y: 0. },
                Vec2 { x: 0., y: -1. },
                Vec2 { x: -1., y: -1. },
            ],
            TetrominoKind::L => [
                Vec2 { x: 0., y: 1. },
                Vec2 { x: 0., y: 0. },
                Vec2 { x: 0., y: -1. },
                Vec2 { x: 1., y: -1. },
            ],
            TetrominoKind::S => [
                Vec2 { x: 0., y: 0. },
                Vec2 { x: 0., y: -1. },
                Vec2 { x: 1., y: 0. },
                Vec2 { x: -1., y: -1. },
            ],
            TetrominoKind::Z => [
                Vec2 { x: 0., y: 0. },
                Vec2 { x: 0., y: -1. },
                Vec2 { x: -1., y: 0. },
                Vec2 { x: 1., y: -1. },
            ],
        };

        for i in 0..4 {
            bundle[i].transform.translation += offset[i].extend(0.) * sizes;
        }
        (bundle, offset)
    }
}

pub fn rotate_cell(pos: Vec2, rotation: &Rotation) -> Vec2 {
    let x = pos.x;
    let y = pos.y;
    match rotation {
        Rotation::Left => Vec2::new(-y, x),
        Rotation::Right => Vec2::new(y, -x),
        Rotation::Flip => Vec2::new(-x, -y),
        Rotation::Neutral => pos,
    }
}

pub fn move_tetromino(
    mut tetromino_query: Query<(&mut Transform, &mut Tetromino), With<Tetromino>>,
    keyboard_input: Res<ButtonInput<KeyCode>>,
    sizes: Res<Sizes>,
    score: Res<Score>,
    time: Res<Time>,
    mut timer: ResMut<PlaceDelay>,
) {
    let (mut transform, mut tetromino) = tetromino_query.single_mut();
    let level: u32 = score.lines as u32 / 10 + 1;
    tetromino.speed = sizes.cell / (0.8 - ((level as f32 - 1.) * 0.007)).powf(level as f32 - 1.);
    let mut delta_dist = tetromino.speed * time.delta_seconds();

    if keyboard_input.just_pressed(KeyCode::KeyW) && tetromino.moveable.down {
        // Sonic drop
        transform.translation.y = tetromino.drop_y_pos;
        tetromino.pos.y = tetromino.drop_y_pos / sizes.cell;
        return;
    }
    if keyboard_input.just_pressed(KeyCode::Space) && tetromino.moveable.down {
        // Hard drop
        transform.translation.y = tetromino.drop_y_pos;
        timer.timer.unpause();
        let rem = timer.timer.remaining();
        timer.timer.tick(rem);
        tetromino.moveable.down = false;
        tetromino.pos.y = tetromino.drop_y_pos / sizes.cell;
        return;
    }
    if keyboard_input.just_pressed(KeyCode::KeyD) && tetromino.moveable.right {
        transform.translation += Vec3::new(sizes.cell, 0., 0.);
        tetromino.pos.x += 1.;
    }
    if keyboard_input.just_pressed(KeyCode::KeyA) && tetromino.moveable.left {
        transform.translation += Vec3::new(-sizes.cell, 0., 0.);
        tetromino.pos.x += -1.;
    }
    if keyboard_input.pressed(KeyCode::KeyS) {
        delta_dist *= 2.;
    }
    if tetromino.moveable.down {
        transform.translation.y -= delta_dist;
        tetromino.pos.y -= delta_dist / sizes.cell;
    } else if transform.translation.y % sizes.cell != 0. {
        transform.translation.y =
            (transform.translation.y / (sizes.cell / 2.)).round() * sizes.cell / 2.;
    }
}

pub fn rotate_tetromino(
    mut tetromino_query: Query<&mut Tetromino>,
    keyboard_input: Res<ButtonInput<KeyCode>>,
) {
    let mut tetromino = tetromino_query.single_mut();
    if keyboard_input.just_pressed(KeyCode::KeyJ) && tetromino.can_rotate.left {
        tetromino.rotation.rotate(Rotation::Left);
    } else if keyboard_input.just_pressed(KeyCode::KeyL) && tetromino.can_rotate.right {
        tetromino.rotation.rotate(Rotation::Right);
    } else if keyboard_input.just_pressed(KeyCode::KeyK) && tetromino.can_rotate.down {
        tetromino.rotation.rotate(Rotation::Flip);
    }
}

fn test_rot_col(
    tetromino_transform: &Transform,
    cell_transform: &Transform,
    collider: &Transform,
    rotation: &Rotation,
) -> bool {
    Aabb2d::new(
        tetromino_transform.translation.truncate()
            + rotate_cell(cell_transform.translation.truncate(), rotation),
        cell_transform.scale.truncate() / 2.01,
    )
    .intersects(&Aabb2d::new(
        collider.translation.truncate(),
        collider.scale.truncate() / 2.01,
    ))
}

pub fn can_rotate(
    cells: Query<&Transform, With<Cell>>,
    colliders: Query<&Transform, With<Collider>>,
    mut tetromino: Query<&mut Tetromino>,
    tetromino_transform: Query<&Transform, With<Tetromino>>,
) {
    let mut tetromino = tetromino.single_mut();
    let tetromino_transform = tetromino_transform.single();
    let mut can_rotate = Directions {
        left: true,
        right: true,
        down: true,
    };
    match tetromino.kind {
        TetrominoKind::O => {
            tetromino.can_rotate = Directions {
                left: false,
                right: false,
                down: false,
            };
            return;
        }
        TetrominoKind::Z | TetrominoKind::S | TetrominoKind::I => {
            can_rotate.down = false;
        }
        _ => (),
    }

    for cell_transform in &cells {
        for collider in &colliders {
            if test_rot_col(
                tetromino_transform,
                cell_transform,
                collider,
                &Rotation::Left,
            ) {
                can_rotate.left = false;
            }

            if test_rot_col(
                tetromino_transform,
                cell_transform,
                collider,
                &Rotation::Right,
            ) {
                can_rotate.right = false;
            }

            if test_rot_col(
                tetromino_transform,
                cell_transform,
                collider,
                &Rotation::Flip,
            ) {
                can_rotate.down = false;
            }
        }
    }
    tetromino.can_rotate = can_rotate;
}

pub fn despawn_tetromino(
    mut commands: Commands,
    tetromino: Query<(&Tetromino, Entity, &Transform), With<Tetromino>>,
    cell_query: Query<&Transform, With<Cell>>,
    mut ev_despawn: EventWriter<TetrominoDespawned>,
    (mut next_tetromino, mut block_grid, mut timer, mut next_state): (
        ResMut<NextTetromino>,
        ResMut<BlockGrid>,
        ResMut<PlaceDelay>,
        ResMut<NextState<GameState>>,
    ),
    (time, sizes): (Res<Time>, Res<Sizes>),
    preview: Query<Entity, With<Preview>>,
) {
    let (tetromino, entity, transform) = tetromino.single();
    if tetromino.moveable.down || !timer.timer.tick(time.delta()).finished() {
        return;
    }

    let cells: Vec<Vec3> = cell_query
        .iter()
        .map(|t| t.translation + transform.translation)
        .collect();

    commands.entity(entity).despawn_recursive();
    Tetromino::spawn(&mut commands, next_tetromino.0.clone(), sizes.cell);
    let ret = Block::spawn_blocks(&mut commands, &tetromino.kind, &cells, sizes.cell);

    for (id, (x, y)) in ret {
        block_grid.0[y as usize][x as usize] = Some(id);
    }

    next_tetromino.0 = TetrominoKind::new();
    commands.entity(preview.single()).despawn_recursive();
    Preview::spawn_preview(&mut commands, &next_tetromino.0, sizes.cell);

    for cell in cells {
        let (_, y) = world_to_grid((cell.x, cell.y), sizes.cell);
        if y > 18 {
            next_state.set(GameState::GameOver);
        }
    }
    timer.timer.pause();
    timer.timer.reset();
    ev_despawn.send_default();
}

pub fn can_move(
    cells: Query<&Transform, With<Cell>>,
    colliders: Query<&Transform, With<Collider>>,
    mut tetromino: Query<&mut Tetromino>,
    tetromino_transform: Query<&Transform, With<Tetromino>>,
    mut timer: ResMut<PlaceDelay>,
    time: Res<Time>,
    sizes: Res<Sizes>,
) {
    let tetromino_transform = tetromino_transform.single();
    let mut moveable = Directions {
        left: true,
        right: true,
        down: true,
    };
    let offset_right = Vec2::new(sizes.cell, 0.);
    let offset_left = Vec2::new(-sizes.cell, 0.);
    let offset_down = Vec2::new(0., -tetromino.single().speed * time.delta_seconds());

    for cell_transform in &cells {
        for collider in &colliders {
            if Aabb2d::new(
                tetromino_transform.translation.truncate()
                    + cell_transform.translation.truncate()
                    + offset_right,
                cell_transform.scale.truncate() / 2.01,
            )
            .intersects(&Aabb2d::new(
                collider.translation.truncate(),
                collider.scale.truncate() / 2.01,
            )) {
                moveable.right = false;
            }

            if Aabb2d::new(
                tetromino_transform.translation.truncate()
                    + cell_transform.translation.truncate()
                    + offset_left,
                cell_transform.scale.truncate() / 2.01,
            )
            .intersects(&Aabb2d::new(
                collider.translation.truncate(),
                collider.scale.truncate() / 2.01,
            )) {
                moveable.left = false;
            }

            if Aabb2d::new(
                tetromino_transform.translation.truncate()
                    + cell_transform.translation.truncate()
                    + offset_down,
                cell_transform.scale.truncate() / 2.01,
            )
            .intersects(&Aabb2d::new(
                collider.translation.truncate(),
                collider.scale.truncate() / 2.01,
            )) {
                moveable.down = false;
            }
        }
    }

    if moveable.down {
        timer.timer.pause();
        timer.timer.reset();
    } else {
        timer.timer.unpause();
    }

    tetromino.single_mut().moveable = moveable;
}

pub fn calculate_hard_drop(
    colliders: Query<&Transform, With<Collider>>,
    cells: Query<&Transform, With<Cell>>,
    mut tetromino: Query<&mut Tetromino>,
    tetromino_transform: Query<&Transform, With<Tetromino>>,
    sizes: Res<Sizes>,
) {
    let size = sizes.cell;
    let tetromino_main_t = (tetromino_transform.single().translation / size).round() * size;
    let mut offset = Vec2::new(0., 0.);
    loop {
        for collider in &colliders {
            for cell_transform in &cells {
                if Aabb2d::new(
                    tetromino_main_t.truncate() + cell_transform.translation.truncate() - offset,
                    cell_transform.scale.truncate() / 2.01,
                )
                .intersects(&Aabb2d::new(
                    collider.translation.truncate(),
                    collider.scale.truncate() / 2.01,
                )) {
                    tetromino.single_mut().drop_y_pos = tetromino_main_t.y - offset.y + size / 2.;
                    return;
                }
            }
        }
        offset.y += size;
    }
}
