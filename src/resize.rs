use crate::{
    grid_to_world, rotate_cell, Block, Cell, GameState, Preview, PreviewBlock, Sizes, Tetromino,
    TetrominoKind, Wall, WallLocation, Walls,
};
use bevy::prelude::*;

pub struct ResizePlugin;

impl Plugin for ResizePlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            Update,
            (
                update_cells,
                update_walls,
                update_blocks,
                update_tetromino,
                update_preview,
                update_preview_blocks,
                update_font_size,
                update_sizes,
            )
                .run_if(in_state(GameState::Playing)),
        )
        .add_systems(
            Update,
            (update_sizes, update_font_size, update_sizes)
                .run_if(in_state(GameState::Menu)),
        )
        .add_systems(
            Update,
            (update_font_size, update_sizes).run_if(in_state(GameState::GameOver)),
        );
    }
}

fn update_sizes(window: Query<&Window>, mut sizes: ResMut<Sizes>) {
    let width = window.single().resolution.width();
    let height = window.single().resolution.height();

    sizes.cell = ((height / 24.).floor()).min((width / 24.).floor());
    sizes.wall = (sizes.cell / 5.).floor();
}

fn update_font_size(mut text_query: Query<&mut Text>, sizes: Res<Sizes>) {
    let cell_size = sizes.cell;
    for mut text in text_query.iter_mut() {
        for section in text.sections.iter_mut() {
            section.style.font_size = cell_size * 2.;
        }
    }
}

fn update_cells(
    mut cells: Query<(&mut Transform, &Cell)>,
    sizes: Res<Sizes>,
    tetromino: Query<&Tetromino>,
) {
    let tetromino = tetromino.single();
    for (mut t, c) in &mut cells {
        t.scale = Vec3::new(sizes.cell, sizes.cell, 1.);
        let mut a = Vec2::new(c.x as f32 * sizes.cell, c.y as f32 * sizes.cell);
        a = rotate_cell(a, &tetromino.rotation);
        t.translation.y = a.y;
        t.translation.x = a.x;
    }
}

fn update_tetromino(
    mut tetromino: Query<(&mut Transform, &Tetromino), With<Tetromino>>,
    sizes: Res<Sizes>,
) {
    let (mut transform, tetromino) = tetromino.single_mut();
    transform.translation.x = tetromino.pos.x * sizes.cell;
    transform.translation.y = tetromino.pos.y * sizes.cell;
}

fn update_blocks(mut blocks: Query<(&mut Transform, &Block), With<Block>>, sizes: Res<Sizes>) {
    for (mut t, b) in &mut blocks {
        t.scale = Vec3::new(sizes.cell, sizes.cell, 1.);
        let (x, y) = grid_to_world((b.x, b.y), sizes.cell);
        t.translation.x = x;
        t.translation.y = y;
    }
}

fn update_preview(
    mut tetromino: Query<(&mut Transform, &Preview), With<Preview>>,
    sizes: Res<Sizes>,
) {
    let (mut transform, tetromino) = tetromino.single_mut();
    transform.translation = match tetromino.kind {
        TetrominoKind::J | TetrominoKind::Z => Vec3::new(
            (tetromino.x - 1.) * sizes.cell,
            tetromino.y * sizes.cell,
            0.,
        ),
        _ => Vec3::new(tetromino.x * sizes.cell, tetromino.y * sizes.cell, 0.),
    };
    transform.translation.x = tetromino.x * sizes.cell;
    transform.translation.y = tetromino.y * sizes.cell;
}

fn update_preview_blocks(
    mut blocks: Query<(&mut Transform, &PreviewBlock), With<PreviewBlock>>,
    sizes: Res<Sizes>,
) {
    for (mut t, b) in &mut blocks {
        t.scale = Vec3::new(sizes.cell, sizes.cell, 1.);
        t.translation.x = b.x * sizes.cell;
        t.translation.y = b.y * sizes.cell;
    }
}

fn update_walls(sizes: Res<Sizes>, mut walls: Query<&mut Transform, With<Wall>>) {
    for mut wall in &mut walls {
        let x = wall.translation.x;
        let location;
        if x > sizes.cell {
            location = Some(WallLocation::Right);
        } else if x < -sizes.cell {
            location = Some(WallLocation::Left);
        } else {
            location = Some(WallLocation::Bottom);
        }
        if let Some(location) = location {
            let walls = Walls::new(&sizes);
            wall.translation = location.position(&walls).extend(0.);
            wall.scale = location.size(&walls, &sizes).extend(1.);
        } else {
            panic!("f location");
        }
    }
}
