use crate::tetromino::{Tetromino, TetrominoKind};
use crate::walls::Collider;
use crate::world_to_grid;
use bevy::prelude::*;

#[derive(Component)]
pub struct Block {
    pub x: u8,
    pub y: u8,
}

impl Block {
    pub fn spawn_blocks(
        commands: &mut Commands,
        kind: &TetrominoKind,
        pos: &[Vec3],
        cell_size: f32,
    ) -> [(Entity, (u8, u8)); 4] {
        let (mut sprites, _) = Tetromino::new_sprites(kind, cell_size);
        let mut ids = Vec::new();
        let mut b_pos = Vec::new();

        for (i, sprite) in sprites.iter_mut().enumerate() {
            sprite.transform.translation = pos[i];
            let (x, y) = world_to_grid(
                (
                    sprite.transform.translation.x,
                    sprite.transform.translation.y,
                ),
                cell_size,
            );
            ids.push(
                commands
                    .spawn((sprite.to_owned(), Block { x, y }, Collider))
                    .id(),
            );
            b_pos.push((x, y))
        }
        [
            (ids[0], b_pos[0]),
            (ids[1], b_pos[1]),
            (ids[2], b_pos[2]),
            (ids[3], b_pos[3]),
        ]
    }
}
